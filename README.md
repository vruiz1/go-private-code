### Go private library
This example shows how to hide part of your golang library.

The goal here is to obfuscate the source code of the go package `myprivatelib/foo`.
External packages can import `foo` and call their functions directly via code while not exposing it's underline implementation.

The idea is: Convert the private go package to a C library which can be later imported by another package using the cgo compiler features.


The repository contains the next files:

- `myprivatelib/foo` is the package we want to hide. Defines a function `DoSomethingPrivate()`

- `myprivatelib/main.go` exposes the features of `DoSomethingPrivate()` by wrapping it with the function `DoSomething()`. The later can be invoked by any external package.

- `mypubliclib/main.go` imports the private library and calls `DoSomething()`



The private package can be built as a dynamic library (.so file) so that we can hide the implementation (it's a binary file) and import the function `DoSomething()` from an external go module.

To achieve this, run:
```bash
cd mypubliclib
make libfoo.so
```
This will create two files:

- `libfoo.so`: The runtime library with the `foo` definitions.
- `libfoo.h`: Contains the function `DoSomething()` declaration.


Now we can build `mypubliclib/main.go`:
```bash
cd mypubliclib
make mypubliclib
```
The above command builds the module main by linking the .so library when compilling the go program.
The resulting binary must be run with `LD_LIBRARY_PATH` pointing to the location of the `libfoo.so` file.
